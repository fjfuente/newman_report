# Newman Report with Postman API

Create reports in folder *newman/* with newman command.

## Configure

Change file config.

```
export POSTMAN_COLLECTION=<my_postman_collection_url>
export POSTMAN_ENV=<my_postman_environment_url>
export POSTMAN_APIKEY=<my_postman_apikey>
export POSTMAN_FOLDER=<my_postman_folder>
export NEWMAN_TITLE_REPORT=<my_title_report>
```

## Use

First load environment variables with configs and run script.

```
source ./config
./report.sh
```
