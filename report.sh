#!/bin/bash

########################################################################
#
# new-report.sh
#
# Created by: Francisco Fuentes
# Date: 25-08-2019
# Email: contacto@francisco-fuentes.cl
#
# Build newman report with newman-reporter-htmlextra and Postman API.
#
########################################################################

newman run ${POSTMAN_COLLECTION}\?apikey\=${POSTMAN_APIKEY} \
--environment ${POSTMAN_ENV}\?apikey\=${POSTMAN_APIKEY} \
--folder ${POSTMAN_FOLDER} \
--reporters cli,htmlextra \
--reporter-htmlextra-title ${NEWMAN_TITLE_REPORT}
